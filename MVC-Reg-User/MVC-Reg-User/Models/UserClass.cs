﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MVC_Reg_User.Models
{
    public class UserClass
    {
        [Required(ErrorMessage ="Nhập Vào UserName")]
        [Display(Name ="Nhập Vào UserName")]
        [StringLength(maximumLength:7,MinimumLength =3,ErrorMessage ="UserName nhập lớn 3 và nhỏ hơn 7")]
        public string Uname { set; get; }

        [Required(ErrorMessage = "Nhập Vào Email")]
        [Display(Name = "Nhập Vào Email")]
       
        public string Uemail { set; get; }
        [Required(ErrorMessage = "Nhập Vào PassWord")]
        [Display(Name = "Nhập Vào PassWord")]
        [DataType(DataType.Password)]
        public string Upwd { set; get; }
        [Required(ErrorMessage = "Nhập Lại PassWord")]
        [Display(Name = "Nhập Lại PassWord")]
        [DataType(DataType.Password)]
        [Compare("Upwd")]
        public string Repwd { set; get; }
        [Required(ErrorMessage = "Lựa Chọn Giới Tính ")]
        [Display(Name = "Giới Tính ")]
        public char Gender { set; get; }
        [Required(ErrorMessage = "Lựa Chọn Hình Ảnh ")]
        [Display(Name = "Image ")]
        public string Uimg { set; get; }
    }
}