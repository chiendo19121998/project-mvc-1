﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JQueryDataTableMVC.Models
{
    [MetadataType(typeof(EmployeeMetData))]
    public partial class Employee
    {
    }
    public class EmployeeMetData
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mời Nhập Vào Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mời Nhập Vào Position")]
        public string Position { get; set; }



    }
}