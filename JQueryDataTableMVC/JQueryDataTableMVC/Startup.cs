﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JQueryDataTableMVC.Startup))]
namespace JQueryDataTableMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
