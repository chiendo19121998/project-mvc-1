﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using layout.Models;
using System.Data.Entity;
namespace layout.Controllers
{
    public class GiaovienController : Controller
    {
        // GET: Giaovien
        public ActionResult Index()
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.gvs.ToList());
            }
        }

        // GET: Giaovien/Details/5
        public ActionResult Details(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.gvs.Where(x => x.id == id).FirstOrDefault());
            }
            return View();
        }

        // GET: Giaovien/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Giaovien/Create
        [HttpPost]
        public ActionResult Create(gv gv)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {

                    dbmodel.gvs.Add(gv);
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Giaovien/Edit/5
        public ActionResult Edit(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.gvs.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: Giaovien/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, gv gv)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    dbmodel.Entry(gv).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Giaovien/Delete/5
        public ActionResult Delete(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.gvs.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: Giaovien/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    gv gv = dbmodel.gvs.Where(x => x.id == id).FirstOrDefault();
                    dbmodel.gvs.Remove(gv);
                    dbmodel.SaveChanges();

                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
