﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using layout.Models;
using System.Data.Entity;
namespace layout.Controllers
{
    public class LopController : Controller
    {
        // GET: Lop
        public ActionResult Index()
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.lops.ToList());
            }
        }

        // GET: Lop/Details/5
        public ActionResult Details(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.lops.Where(x => x.id == id).FirstOrDefault());
            }
            return View();
        }

        // GET: Lop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Lop/Create
        [HttpPost]
        public ActionResult Create(lop lop)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {

                    dbmodel.lops.Add(lop);
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Lop/Edit/5
        public ActionResult Edit(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.lops.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: Lop/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, lop lop)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    dbmodel.Entry(lop).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Lop/Delete/5
        public ActionResult Delete(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.lops.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: Lop/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    lop lop = dbmodel.lops.Where(x => x.id == id).FirstOrDefault();
                    dbmodel.lops.Remove(lop);
                    dbmodel.SaveChanges();

                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
