﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using layout.Models;
using System.Data.Entity;
namespace layout.Controllers
{
    public class SinhVienController : Controller
    {
        // GET: SinhVien
        public ActionResult Index()
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.svs.ToList());
            }
        }

        // GET: SinhVien/Details/5
        public ActionResult Details(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.svs.Where(x => x.id == id).FirstOrDefault());
            }
            return View();
        }

        // GET: SinhVien/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SinhVien/Create
        [HttpPost]
        public ActionResult Create(sv sv)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {

                    dbmodel.svs.Add(sv);
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SinhVien/Edit/5
        public ActionResult Edit(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.svs.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: SinhVien/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, sv sv)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    dbmodel.Entry(sv).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SinhVien/Delete/5
        public ActionResult Delete(int id)
        {
            using (MhEntities dbmodel = new MhEntities())
            {
                return View(dbmodel.svs.Where(x => x.id == id).FirstOrDefault());
            }
        }

        // POST: SinhVien/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (MhEntities dbmodel = new MhEntities())
                {
                    sv sv = dbmodel.svs.Where(x => x.id == id).FirstOrDefault();
                    dbmodel.svs.Remove(sv);
                    dbmodel.SaveChanges();

                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
